# GIS

## Dependencies:
* matplotlib
* pandas
* geopandas
* descartes
* geopy
* shapely
* ipywidgets

## Data
* https://www.suche-postleitzahl.org/downloads
